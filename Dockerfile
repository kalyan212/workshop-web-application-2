FROM maven:3.5-jdk-11

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN mvn -Dmaven.repo.local=.m2/repository -DskipTests clean package

ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "mvn -Dmaven.repo.local=.m2/repository -DskipTests clean -Dserver.port=${PORT} spring-boot:run" ]
